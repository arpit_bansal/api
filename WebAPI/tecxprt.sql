create database tecxprtdb;
use tecxprtdb;


create table admin (
	id bigint NOT NULL AUTO_INCREMENT,
    full_name varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    isActive BOOLEAN  not null,
    isDelete BOOLEAN  not null,
    primary key(id),
    foreign key(user_type) references user_type(id)
    );
    
    
    
use tecxprtdb;
CREATE USER 'tecxpert'@'localhost' IDENTIFIED WITH mysql_native_password BY 'tecxpert';
GRANT ALL PRIVILEGES ON *.* TO 'tecxpert'@'localhost';
    
    
    