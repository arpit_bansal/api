from sqlalchemy import Column, Integer, String,Boolean
from .database import Base
from enum import Enum
import datetime

class UserInfo(Base):
    __tablename__ = "admin"

    id = Column(Integer, primary_key=True, index=True)
    full_name = Column(String)
    Description = Column(String)
    email = Column(String, unique=True)
    password= Column(String)
    isActive = Column(Boolean)
    isDelete= Column(Boolean)



class TxUserInfo(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    full_name = Column(String)
    user_type=Column(Integer)
    email = Column(String, unique=True)
    password= Column(String)
    isActive = Column(Boolean)
    isDelete= Column(Boolean)



class ProductInfo(Base):
    __tablename__ = "product"

    id = Column(Integer, primary_key=True, index=True)
    ProductName = Column(String)
    Price=Column(Integer)
    MRP = Column(Integer)
    MfgDate = Column(Integer)
    ExpDate = Column(Integer)
    Description= Column(String)
    ProductCode=Column(String)





    id = Column(Integer, primary_key=True, index=True)
    message=Column(String)
