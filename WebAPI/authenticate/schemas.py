from typing import List, Optional
from pydantic import BaseModel
import datetime
import string
import random

class AdminCreate(BaseModel):
    email: str
    password: str
    Description: str
    full_name: str
    isActive:bool
    isDelete:bool

class UserCreate(BaseModel):
    email: str
    password: str
    user_type: int
    full_name: str
    isActive:bool
    isDelete:bool

class UserEdit(BaseModel):
    email: str
    password: str
    user_type: int
    full_name: str
    isActive:bool
    isDelete:bool

class UserSignIn(BaseModel):
    email: str
    password: str

class UserInfo(BaseModel):
    id:int
    full_name: str
    password: str
    email: str
    user_type: int
    isActive: bool
    isDelete:bool

class UserInfoUpdate(BaseModel):
    full_name: str
    email: str
    user_type: int
    isActive: bool
    isDelete:bool


class UserInfoId(BaseModel):
    id:int

class Config:
    orm_mode = True

class ProductCreate(BaseModel):
    ProductName: str
    Price: float
    MRP: float
    MfgDate:datetime.datetime
    ExpDate: datetime.datetime
    Description: str
    ProductCode:str


class ProductId(BaseModel):
    id:int


class ProductInfo(BaseModel):
    id:int
    ProductName: str
    Price: float
    MRP: float
    MfgDate: datetime.datetime
    ExpDate: datetime.datetime
    Description: str
    ProductCode: str





class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None

class Settings(BaseModel):
    authjwt_secret_key: str = "secret"