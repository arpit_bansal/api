from sqlalchemy.orm import Session
from authenticate import models
from authenticate import schemas
import string
import random
from passlib.context import CryptContext


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
def get_password_hash(password):
    return pwd_context.hash(password)
def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_user_by_email(db: Session, email: str):
    return db.query(models.UserInfo).filter(models.UserInfo.email == email).first()
def get_txuser_by_email(db: Session, email: str):
    return db.query(models.TxUserInfo).filter(models.TxUserInfo.email == email).first()
def get_user_info(db: Session, email: str):
    return db.query(models.UserInfo).filter(models.UserInfo.email==email)
def get_user_type(db:Session):
    return db.query(models.UserTypeInfo).all()
def get_txuser(db:Session):
    return db.query(models.TxUserInfo).all()

def get_txuser_by_email(db: Session, email: str):
    return db.query(models.TxUserInfo).filter(models.TxUserInfo.email == email).first()


def get_txuser_by_id(db: Session, id: int):
    return db.query(models.TxUserInfo).filter(models.TxUserInfo.id == id).first()


def get_product_by_productname(db: Session, ProductName: str):
    return db.query(models.ProductInfo).filter(models.ProductInfo.ProductName == ProductName).first()

def create_admin(db: Session, user: schemas.AdminCreate):
    hashpassword=get_password_hash(user.password)
    db_user = models.UserInfo(email=user.email, password= hashpassword, full_name= user.full_name,isDelete= user.isDelete ,isActive= user.isActive, Description=user.Description)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def create_user(db: Session, user: schemas.UserCreate):
    hashpassword=get_password_hash(user.password)
    db_user = models.TxUserInfo(email=user.email, password= hashpassword, user_type= user.user_type, full_name= user.full_name,isDelete= user.isDelete ,isActive= user.isActive)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user




def delete_user(db: Session, user: schemas.UserInfoId):
    db_user = db.query(models.TxUserInfo).filter(models.TxUserInfo.id == user.id).first()
    db.delete(db_user)
    db.commit()
    return print("User Deleted")

#
def edit_user(db: Session, user: schemas.UserInfo):
    db_user1 = models.TxUserInfo(email=user.email,password= user.password,id=user.id,  user_type=user.user_type,full_name=user.full_name, isDelete=user.isDelete, isActive=user.isActive)
    db.merge(db_user1)
    db.commit()
    return db_user1


def new_product(db: Session, user: schemas.ProductCreate):
    code = ''.join(random.choices(string.ascii_uppercase +string.digits, k=5))
    db_user = models.ProductInfo(ProductName=user.ProductName, ProductCode=code,Price= user.Price, MRP= user.MRP, MfgDate= user.MfgDate,ExpDate= user.ExpDate ,Description= user.Description)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user




def get_product_list(db:Session):
    return db.query(models.ProductInfo).all()

def delete_product(db: Session, user: schemas.ProductId):
    db_user = db.query(models.ProductInfo).filter(models.ProductInfo.id == user.id).first()
    db.delete(db_user)
    db.commit()
    return print("Product Deleted")

def edit_product(db: Session, user: schemas.ProductInfo):
    db_user1 = models.ProductInfo(id=user.id,ProductName=user.ProductName, Price=user.Price, MRP=user.MRP, MfgDate=user.MfgDate, ExpDate=user.ExpDate, Description=user.Description)
    db.merge(db_user1)
    db.commit()
    return db_user1