import uvicorn
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session
from authenticate import crud
from authenticate.oauth2 import get_current_user
from authenticate import models
from authenticate import schemas
from authenticate.database import SessionLocal, engine
from passlib.context import CryptContext
from fastapi.security import OAuth2PasswordRequestForm
from fastapi import FastAPI, HTTPException, Depends, Request,  File, UploadFile
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from fastapi.responses import  FileResponse
import datetime

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

origins = [
    "http://localhost:8080/signin",
    "http://localhost:3000/login",
    "http://localhost:3000/",
    "http://localhost:3000/#/login",
    "http://localhost",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")




def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@AuthJWT.load_config
def get_config():
    return schemas.Settings()

@app.exception_handler(AuthJWTException)
def authjwt_exception_handler(request: Request, exc: AuthJWTException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.message}
    )


# admin table sign in for react app
@app.post("/signin/", tags=["Authenticate"])
def sign_in(request: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db),Authorize: AuthJWT = Depends()):
    db_user = crud.get_user_by_email(db, email=request.username)
    if not db_user:
        raise  HTTPException(status_code=400, detail="User not exists")
    elif not pwd_context.verify( request.password, db_user.password):
        raise HTTPException(status_code=401, detail="password not match")
    access_token = Authorize.create_access_token(subject=db_user.email,fresh=True)
    refresh_token = Authorize.create_refresh_token(subject=db_user.email)
    return {"access_token": access_token,"refresh_token" :refresh_token}


@app.post('/refresh', tags=["Authenticate"])
def refresh(Authorize: AuthJWT = Depends()):
    Authorize.jwt_refresh_token_required()
    current_user = Authorize.get_jwt_subject()
    new_access_token = Authorize.create_access_token(subject=current_user)
    return {"access_token": new_access_token}




@app.post("/admin-add/", tags=["Authenticate"])
def create_admin( user: schemas.AdminCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Username already registered")
    return crud.create_admin(db=db, user=user)

# @app.get("/user-types/")
# def user_type(db: Session = Depends(get_db)):
#     db_user = crud.get_user_type(db)
#     return db_user


@app.post("/user-add/", tags=["User"])
def create_user( user: schemas.UserCreate, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    db_user = crud.get_txuser_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Username already registered")
    return crud.create_user(db=db, user=user)

@app.get("/user-info/", tags=["User"])
def user_type(db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    db_user = crud.get_txuser(db)
    return db_user

@app.delete("/user-delete/", tags=["User"])
def delete_user( user: schemas.UserInfoId, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    db_user = user.id
    if db_user:
        return crud.delete_user(db=db, user=user)



@app.put("/user-update/", tags=["User"])
def update_user( user: schemas.UserInfo, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    db_user = user.id
    if db_user:
        return crud.edit_user(db=db, user=user)



@app.post("/product-add/", tags=["Product"])
def add_product( user: schemas.ProductCreate, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    return crud.new_product(db=db, user=user)

@app.get("/product-list/", tags=["Product"])
def product_list(db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    db_user = crud.get_product_list(db)
    return db_user



@app.delete("/product-delete/", tags=["Product"])
def delete_product( user: schemas.ProductId, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    db_user = user.id
    if db_user:
        return crud.delete_product(db=db, user=user)


@app.put("/product-edit/", tags=["Product"])
def update_product( user: schemas.ProductInfo, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    db_user = user.id
    if db_user:
        return crud.edit_product(db=db, user=user)




@app.get("/chatbot/", tags=["Chatbot"])
def chatbot():
    return FileResponse("files/Steps.json")


@app.get("/convo/", tags=["Chatbot"])
def chatbot():
    return FileResponse("files/convo.json")



@app.post("/upload")
async def fileUpload(file: bytes = File(...)):
    print( len(file))
    x = datetime.datetime.now()
    y=(x.strftime("%d"+"%I"+"%M"+"%S"+"%f"))
    f = open("resume/resume"+y+".txt", "wb")
    f.write(file)
    f.close()
    return {"file resume"+y+".txt is saved on server" }


# @app.post("/record/", tags=["Chatbot"])
# def record():
#     record = open("./files/messages.text", "a+")
#     record.write("hello \n")
#     record.close()
#
#     return {"hello":"world"}


if __name__ == "__main__":

    uvicorn.run(app, host="139.59.89.143", port=7000)

